import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

class PasswordGeneratorTest {

    @Test
    void given_whenGeneratePassword_thenReturnNotNullResult() {
        PasswordGenerator pg = new PasswordGenerator();
        String pass = pg.generatePassword();
        Assertions.assertFalse(pass == null);
    }

    @Test
    void given_whenGeneratePassword_thenReturnPasswordWithLengthEight() {
        PasswordGenerator pg = new PasswordGenerator();
        String pass = pg.generatePassword();
        int expectedLength = 8;
        Assertions.assertTrue(pass.length() == expectedLength, "expected length " + expectedLength + " but found " + pass.length());
    }

    @Test
    void given_whenGeneratePassword_thenReturnPasswordHavingTwoUpperCaseLettersAndFourNumbersAndTwoSymbols() {
        PasswordGenerator pg = new PasswordGenerator();
        String pass = pg.generatePassword();

        Assertions.assertTrue(isFollowNoOfCharsRules(pass), "The Password doesn't follow the number of characters rules.");
    }

    @Test
    void given_whenGenerateTenPassword_thenReturnTenThousandDistinctPasswords() {
        PasswordGenerator pg = new PasswordGenerator();
        Set<String> passSet = new HashSet<>();
        for (int i = 0; i < 10000; i++) {
            String pass = pg.generatePassword();
            if (!isFollowNoOfCharsRules(pass))
                break;
            passSet.add(pass);
        }
        int expectedSize = 10000;
        int actualSize = passSet.size();
        Assertions.assertEquals(expectedSize, actualSize, "expected 10000 different passwords but found only " + actualSize);
    }

    private boolean isFollowNoOfCharsRules(String pass) {
        int ltr = 0;
        int num = 0;
        int sym = 0;
        for (int i = 0; i < pass.length(); i++) {
            char current = pass.charAt(i);
            if (Character.isDigit(current)) num++;
            else if (Character.isAlphabetic(current)) ltr++;
            else sym++;
        }
        return (num == 4 && ltr == 2 && sym == 2);
    }
}