import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class PasswordGeneratorTask {

    public static void main(String... args){
        PasswordGenerator pg = new PasswordGenerator();
        for (int i = 1; i <= 10000; i++) {
            System.out.print(pg.generatePassword() + " ");
            if (i%20 == 0) System.out.println();
        }
    }
}
