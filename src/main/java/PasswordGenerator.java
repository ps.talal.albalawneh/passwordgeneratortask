import java.security.SecureRandom;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PasswordGenerator {
    private final char[] UPPER_CASES = {'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', 'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'Z', 'X', 'C', 'V', 'B', 'N', 'M'};
    private final int[] NUMBER = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    private final char[] SYMBOL = {'_', '$', '#', '%'};
    private ArrayList<String> generatedPassword = new ArrayList<>();

    public String generatePassword() {
        String password = getPasswordChars();
        password = checkPasswordRoles(password);

        return password;
    }

    private String checkPasswordRoles(String password) {
        do {
            password = stringShuffle(password);
        } while (!isValidPassword(password));

        generatedPassword.add(password);
        return password;
    }

    private boolean isValidPassword(String password) {
        return !(generatedPassword.contains(password) || isFollowedChars(password));
    }

    private String getPasswordChars() {
        SecureRandom secureRandom = new SecureRandom();
        StringBuilder sequence = new StringBuilder();
        for (int i = 0; i < 2; i++) {
            int rnd = secureRandom.nextInt(UPPER_CASES.length);
            sequence.append(UPPER_CASES[rnd]);
        }
        for (int i = 0; i < 2; i++) {
            int rnd = secureRandom.nextInt(SYMBOL.length);
            sequence.append(SYMBOL[rnd]);
        }
        for (int i = 0; i < 4; i++) {
            int rnd = secureRandom.nextInt(NUMBER.length);
            sequence.append(NUMBER[rnd]);
        }
        return sequence.toString();
    }

    private String stringShuffle(String text) {
        List<Character> pass = text.chars().mapToObj(c -> (char) c).collect(Collectors.toList());
        StringBuilder res = new StringBuilder();
        IntStream.range(0, text.length()).forEach((index) -> {
            int rnd = new SecureRandom().nextInt(pass.size());
            res.append(pass.get(rnd));
            pass.remove(rnd);
        });
        return res.toString();
    }

    private boolean isFollowedChars(String pass) {
        for (int i = 0; i < pass.length() - 1; i++) {
            char first = pass.charAt(i);
            char second = pass.charAt(i + 1);
            if (isSameGroupAndFollowEachOther(first, second))
                return true;
        }
        return false;
    }

    private boolean isSameGroupAndFollowEachOther(char first, char second) {
        if (Character.isDigit(first) && Character.isDigit(second)) {
            return true;
        }
        if (Character.isAlphabetic(first) && Character.isAlphabetic(second)) {
            return true;
        }
        if(isSymbolsFollowsEachOther(first, second)){
            return true;
        }
        return false;
    }

    private boolean isSymbolsFollowsEachOther(char first, char second) {
        return (first == '_' || first == '$' || first == '#' || first == '%')
                && (second == '_' || second == '$' || second == '#' || second == '%');
    }
}
